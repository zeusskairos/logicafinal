% puzzle exemplo

numero([1,2,3,4,5,6,7,8,9]). % números possiveis

gerar(L) :- numero(N),
			gera_linha(N,L).

gera_linha(l,[N1,N2,N3,N4]):- select(N1,L,L1),
							  select(N2,L1,L2),
							  select(N3,L2,L2),
							  select(N4,L3,_).
testar(N1,7,N3,N4]):- N1<7, %
					  N1>N3, %
					  N4 is N3+7, %
					  N1 is N3+1,
					  1 is N1 mod 2.
puzzle(L):- gerar(L), testar(L).
