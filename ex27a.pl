:-[ex27b]. % carregar o sistema de inferencia

% situação actual:
story(ivo,[
			act(A, mbuild,[ivo,zodiac,_,_]),
			act(B, atrans,[ivo,_,_,carla)],
			act(C, ingest,[ivo,pizza,_,_])
		  ]).

% guião genérico:
script(cinema,[		act(1,ptrans,[Actor,Actor,_,Bilheteira])
					act(2,attend,[Actor,Filme,_,_]),
					act(3,mbuild,[Actor,Filme,_,_]),
					act(4,atrans,[Actor,'5euros',Actor,Caixa]),
					act(5,atrans,[Actor,Bilhete,Caixa,Actor]),
					act(6,ptrans,[Actor,Actor,Bilheteira,Sala]),
					act(7,mtrans,[Actor,Filme,_,_]),
					act(8,ptrans,[Actor,Actor,Sala,Restauracao]),
					act(9,ingest,[Actor,Jantar,_,Actor])
			]).

trigger(carla,cinema).
