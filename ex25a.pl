:- [ex25b].

initial(b([1,2],[3,4])).

final(b([1,3],[_,_])).
final(b([3,1],[_,_])).
final(b([2,4],[_,_])).
final(b([4,2],[_,_])).

transition(b(L,[A|R]),ba:[A],b([A|L],R)).
transition(b(L,[A,B|R]),ba:[A,B],b([A,B|L],R)).
transition(b(L,[A,B,C|R]),ba:[A,B,C],b([A,B,C|L],R)).

transition(b([A|R],L),ab:[A],b(R,[A|L])).
transition(b([A,B|R],L),ab:[A,B],b(R,[A,B|L])).
transition(b([A,B,C|R],L),ab:[A,B,C],b(R,[A,B,C|L])).
