% Executado automaticamente quando o Prolog executa este ficheiro:
:-dynamic(fact/1), % definir fact como dinamico
 [ex21b,ex21c,ex21d]. % carregar todos sistemas de inferencia

% Base de Conhecimento, alínea 1
if mineral and duro then berilo.
if berilo or oxido_aluminio then precioso.
if precioso and verde then esmeralda.
if precioso and vermelho then rubi.
if oxido_aluminio and nao_verde_vermelho then safira.

% Base de Dados (os factos actuais), alínea 2
fact(mineral).
fact(oxido_aluminio).
% nota: neste caso também se poderia usar: fact(não_verde_vermelho).
%		contudo, a seguinte definição é mais genérica, sendo que
%		funciona bem quando por exemplo for fact(verde).
fact(nao_verde_vermelho):- \+ fact(vermelho), \+ fact(verde).

% Classificar o mineral via backward chaining:
backward:- demo(safira). % testa se demo de safira é verdade
% Classificar o mineral via backward chaining mas com explicação:
proof(P):- demo(safira,P).
% Classificar o mineral via forward chaining:
forward:- demo. % gera todos os factos que pode provar
