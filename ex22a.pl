% Executando automaticamente quando o Prolog executa este ficheiro:
:-dynamic(fact/1), % definir fact como dinamico
 [ex22b]. % carregar todos sistemas de inferência

% Base de Conhecimento, alínea a)
if portatil and homem then tecnologico:0.75.
if mulher and sem_filhos then romantico:1.0.
if natal and codigo or vinci then policial:0.8.
if natal or mulher then poemas:0.20.
if romantico or poemas then literario:0.9.

% Base de Dados (os factos actuais), alínea b)
fact(natal:1).
fact(mulher:0.5).
fact(homem:0.5).
fact(portatil:1.0).
fact(sem_filhos:1.0).
