% directo
demo(Frame,Slot,Valor)
:- frame(Frame,Slot,Valor).
% herança
demo(Frame,Slot,Valor)
:- super_frame(Frame,Superframe), demo(Superframe,Slot,Valor).
super_frame(Frame,Superframe):- frame(Frame,isa,Superframe).
super_frame(Frame,Superframe):-
frame(Frame,instanceof,Superframe).

